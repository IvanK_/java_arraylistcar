import java.io.*;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.*;
import java.nio.charset.StandardCharsets;

public class Car {

    public String model;
    public Integer year;
    public String color;
    public Integer price;

    public Car() {
        setModel("");
        setYear(0);
        setColor("");
        setPrice(0);
    }

    public Car(String model, Integer year, String color, Integer price) {
        setModel(model);
        setYear(year);
        setColor(color);
        setPrice(price);
    }

    //model getter and setter
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    //year getter and setter
    public Integer getYear() {
        return year;
    }
    public void setYear(Integer year) {
        this.year = year;
    }

    //color getter and setter
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    //price getter and setter
    public Integer getPrice() {
        return price;
    }
    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", year=" + year +
                ", color='" + color + '\'' +
                ", price=" + price +
                '}';
    }

    //method to read and create objects from the file
    public static Car[] read_file(String filename) {
        Car[] city_array = new Car[10];
        String[] line = new String[10];
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            int i=0;
            while ((line[i] = br.readLine()) != null) {
                String[] split = line[i].split("/");
                city_array[i] = new Car();
                city_array[i].model=split[0];
                city_array[i].year= Integer.parseInt(split[1]);
                city_array[i].color=split[2];
                city_array[i].price= Integer.parseInt(split[3]);
                i++;
            }
        } catch (Exception e) {}
        return city_array;
    }


}
