import java.lang.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Iterator;

public class ExecCar {
    public static void main(String[] args) {
        Car[] cars;
        cars = Car.read_file("cars.txt");

        //dispaly read cars from the file
        for (int i = 0; i < cars.length; i++)
            System.out.println(cars[i].toString());

        ArrayList<Car> carArray = new ArrayList<Car>();

        //additing cars to the ArrayList
        for (int i = 0; i < cars.length; i++)
            carArray.add(cars[i]);

        System.out.println(carArray);

        //asking what model to be removed
        System.out.println("What model to be removed?");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        //iterator
        Iterator<Car> iterator = carArray.iterator();


        //crated clone of collection - for removing (as buffer)
        ArrayList<Car> car_clones = new ArrayList<Car>();

        //search for cars to be removed from the basic collection
        while (iterator.hasNext()) {
            Car car = iterator.next();
            if (car.getModel().equals(input)) {
                System.out.println("This car is going to be removed: " + car);
                car_clones.add(car);
            }
        }

        //removing from the basic colletions based on cloned cars
        carArray.removeAll(car_clones);

        System.out.println("Print ArrayList after removing");
        System.out.println(carArray);

    }
}
